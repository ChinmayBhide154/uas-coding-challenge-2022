import math
from numpy import linalg as LNG
import numpy as np


class Circle:
    def __init__(self, radius, x, y):
        self.radius = radius
        self.x = x
        self.y = y

    def is_inside_circle(self, x, y):
        distance = abs(math.sqrt(((x - self.x) * (x - self.x)) + ((y - self.y) * (y - self.y))))
        if distance < self.radius:
            return True

        return False


class Line:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def get_slope(self):
        return (self.y2 - self.y1) / (self.x2 - self.x1)

    def get_tangent_slope(self):
        return (-1) / self.get_slope()

    def get_tangent_slope_unit(self):
        x_comp = self.get_tangent_slope()


def find_number_of_intersections(line, circle):
    p1_line = np.array([line.x1, line.y1])
    p2_line = np.array([line.x2, line.y2])
    p3_circle = np.array([circle.x, circle.y])
    num_area_points = 0

    distance = abs(np.cross(p2_line - p1_line, p3_circle - p1_line) / LNG.norm(p2_line - p1_line))
    if distance > circle.radius:
        return 0

    elif distance < circle.radius:
        if circle.is_inside_circle(line.x1, line.y1):
            num_area_points += 1

        if circle.is_inside_circle(line.x2, line.y2):
            num_area_points += 1

        return (2 - num_area_points)

    else:
        return 1


circle = Circle(5, 0, 0)
line = Line(-4, 0, 0, 0)
num_intersections = find_number_of_intersections(line, circle)
print('The number of intersections are: {num_intersections}'.format(num_intersections=num_intersections))